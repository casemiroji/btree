all: compile

compile:
	@gcc -o main.run *.c -Wall
run:
	@./main.run
debug:
	@gcc -Wall -o main.out -g *.c  
clean:
	@rm main.run *.dat main.out
valgrind:
	valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --num-callers=20 --track-fds=yes --track-origins=yes ./main.run
zip:
	@zip -r entrega.zip *.c *.h Makefile
