#ifndef __BTREE__
#define __BTREE__

// TOOD includes
#include <stdio.h>
#include <string.h>     
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>



#define BTREEFILENAME "tree.dat"

#define PAGESIZE 4096
#define TREE_HEADER (1*PAGESIZE)
#define MAXKEYS 204
#define AUX_FIELDS_SIZE_ON_PAGE (2+1) /*number of keys and "is leaf" bool*/
#define FREE_SPACE_ON_PAGE (PAGESIZE - ((MAXKEYS*4)+(MAXKEYS*8)+((MAXKEYS+1)*8)+3))

#define RECORD_SIZE (sizeof(int) + sizeof(long))



typedef struct record{
    int key;
    long recordRRN;
}record;

typedef struct page{
    record *records;
    long *childs;
    short numberOfKeys;
    bool isLeaf;
    long pageRRN;
} btPage;

typedef struct promotedkey{
    int key;
    long recordRRN;
    long childs[2];
} promotedKey;

btPage *getOrCreateRoot(FILE *);
btPage *getRoot(FILE *fp);
int bTreeInsert(record *, btPage **, FILE *);
long bTreeSelect(btPage *, int, FILE *);
int bTreeSearch(FILE *fp, long RNN, int key, int *RNNFound, int *posFound);

void printPageNode(btPage *page);
void printPromotedKey(promotedKey *key);
btPage *getPage(long RRN, FILE *fp);
void freePage(btPage *page);
long getTreeHeader(FILE *fp);
#endif
